import 'package:moor/moor.dart';

class ToDoTable extends Table{

  TextColumn get uid => text()();
  TextColumn get title => text()();
  BoolColumn get isDone => boolean()();

  @override
  Set<Column> get primaryKey => {uid};

}