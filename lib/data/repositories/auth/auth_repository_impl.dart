import 'dart:async';

import 'package:injectable/injectable.dart';
import 'package:snowtodoapp/data/data_sources/auth/auth_local_data_source.dart';
import 'package:snowtodoapp/data/data_sources/auth/auth_remote_data_source.dart';
import 'package:snowtodoapp/data/mappers/index.dart';
import 'package:snowtodoapp/domain/entities/auth_entity.dart';
import 'package:snowtodoapp/domain/entities/user_entity.dart';
import 'package:snowtodoapp/domain/repositories/auth/auth_repository.dart';

@Singleton(as: AuthRepository)
class AuthRepositoryImpl implements AuthRepository {
  final AuthRemoteDataSource _remote;
  final AuthLocalDataSource _local;
  StreamController<UserEntity> _userStream;

  AuthRepositoryImpl(this._remote, this._local){
    _userStream = StreamController<UserEntity>.broadcast(onListen: _onListenUser);
  }

  void _onListenUser() async{
    _userStream.add(await getUser());
  }

  @override
  Stream<UserEntity> getUserStream() => _userStream.stream;

  @override
  Future<UserEntity> loginUserEmail(
    String email,
    String password, {
    bool saveAuthInfo = true,
  }) async {
    final userEmail = await _remote.loginUserEmail(email, password);
    if (saveAuthInfo) {
      _local
          .saveAuthInfo(AuthEntity(email: email, password: password).toJson());
    }
    var user = userEmail == null ? null : UserEntity(email: userEmail);
    _userStream.add(user);
    return user;
  }

  @override
  Future<void> logoutUser() async {
    await _local.deleteAuthInfo();
    await _local.resetDb();
    _userStream.add(null);
    return _remote.logoutUser();
  }

  @override
  Future<UserEntity> registerUserEmail(String email, String password) async {
    final userEmail = await _remote.registerUserEmail(email, password);
    _local.saveAuthInfo(AuthEntity(email: email, password: password).toJson());
    var user = UserEntity(email: userEmail);
    _userStream.add(user);
    return user;
  }

  @override
  Future<UserEntity> getUser() async {
    final authInfoString = await _local.getAuthInfo();
    if(authInfoString == null || authInfoString.isEmpty) {
      _userStream.add(null);
      return null;
    }
    final authInfo = AuthEntity().fromJson(authInfoString);
    return loginUserEmail(
      authInfo.email,
      authInfo.password,
      saveAuthInfo: false,
    );
  }
}
