import 'dart:async';

import 'package:injectable/injectable.dart';
import 'package:snowtodoapp/data/data_sources/todo_list/todo_list_local_data_source.dart';
import 'package:snowtodoapp/data/data_sources/todo_list/todo_list_remote_data_source.dart';
import 'package:snowtodoapp/data/helpers/network_bound_resources.dart';
import 'package:snowtodoapp/data/helpers/resource.dart';
import 'package:snowtodoapp/data/mappers/index.dart';
import 'package:snowtodoapp/domain/entities/todo_entity.dart';
import 'package:snowtodoapp/domain/repositories/todo_list/todo_list_repository.dart';

@Injectable(as: TodoListRepository)
class TodoListRepositoryImpl implements TodoListRepository {
  final TodoListRemoteDataSource _remoteDataSource;
  final TodoListLocalDataSource _localDataSource;

  TodoListRepositoryImpl(this._remoteDataSource, this._localDataSource);

  @override
  Stream<Resource<List<ToDoItem>>> toDoItemsStream() {
    return NetworkBoundResources<List<ToDoItem>, List<ToDoItem>>().asStream(
      loadFromDb: _localDataSource.allActiveToDoItemsStream,
      shouldFetch: (data) => data == null || data.isEmpty,
      createCall: _remoteDataSource.getPendingToDoItems,
      processResponse: (result) => result,
      saveCallResult: _localDataSource.saveToDoItemList,
    );
  }

  @override
  Future<Resource<List<ToDoItem>>> getToDoList() async {
    return NetworkBoundResources<List<ToDoItem>, List<ToDoItem>>().asFuture(
      loadFromDb: _localDataSource.getToDoList,
      shouldFetch: (data) => data == null || data.isEmpty,
      createCall: _remoteDataSource.getPendingToDoItems,
      saveCallResult: _localDataSource.saveToDoItemList,
    );
  }

  @override
  Future<Resource<ToDoItem>> setTodoItemAsFinished(ToDoItem toDoItem) async {
    return NetworkBoundResources<ToDoItem, ToDoItem>().asFuture(
      loadFromDb: () => toDoItem.copyWith(isDone: !toDoItem.isDone),
      shouldFetch: (data) => true,
      createCall: () => _remoteDataSource.setTodoItemAsFinished(
          toDoItem.copyWith(isDone: !toDoItem.isDone).toJson),
      saveCallResult: _localDataSource.updateToDoItem,
    );
  }

  @override
  Future<Resource<ToDoItem>> addNewToDoItem(ToDoItem toDoItem) async {
    return Resource.asFuture<ToDoItem>(() async => await _remoteDataSource
        .addNewToDoItem(toDoItem.toJson)
        .then(_localDataSource.saveToDoItem));
  }
}
