import 'package:projectbasesnow/config/flavor_config.dart';

import '../../flavor_values.dart';
import 'features.dart';

abstract class Constants {
  static FlavorValuesApp get flavor => FlavorConfig.values();

  static final flavorDev = FlavorValuesApp(
    baseUrl: localhost,
    features: () => Features.devRemote,
  );

  static final flavorQa = FlavorValuesApp(
    baseUrl: localhost,
    features: () => Features.qaRemote,
  );

  static final flavorProd = FlavorValuesApp(
    baseUrl: localhost,
    features: () => Features.prodRemote,
  );

  static const localhost = 'http://10.0.2.2:1337';
}

FlavorValuesApp get flavor => Constants.flavor;