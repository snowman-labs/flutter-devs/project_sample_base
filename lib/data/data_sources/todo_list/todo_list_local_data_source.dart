import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:injectable/injectable.dart';
import 'package:snowtodoapp/data/local/dao/todo_dao.dart';
import 'package:snowtodoapp/data/local/hive_client.dart';
import 'package:snowtodoapp/data/local/moor_db.dart';
import 'package:snowtodoapp/data/mappers/index.dart';
import 'package:snowtodoapp/domain/entities/todo_entity.dart';

@injectable
class TodoListLocalDataSource {

  final HiveClient _hiveClient;
  final ToDoDao _toDoDao;
  static const String box = "todo-items";

  TodoListLocalDataSource(this._hiveClient, this._toDoDao);

  Stream<List<ToDoItem>> allActiveToDoItemsStream(){
    return _toDoDao.allActiveToDoItemsStream.transform(
        StreamTransformer.fromHandlers(
            handleData: (entries, sink){
              sink.add(entries.map<ToDoItem>((entry) => ToDoItem().fromTable(entry)).toList());
            },
            handleError: (error, stacktrace, sink){
              debugPrint(error);
            }
    ));
  }

  Future<List<ToDoItem>> getToDoList() async {
    final res = await _hiveClient.getAll(box);
    return res.isEmpty
        ? []
        : res
            .map((e) => ToDoItem().fromJson(Map<String, dynamic>.from(e)))
            .toList();
  }

  Future<ToDoItem> updateToDoItem(ToDoItem todoItem) async {
    await _toDoDao.updateToDo(todoItem.toTable());
    return todoItem;
  }

  Future<ToDoItem> saveToDoItem(ToDoItem toDoItem) async {
    await _toDoDao.insertToDo(toDoItem.toTable());
    return toDoItem;
  }

  Future<void> saveToDoItemList(List<ToDoItem> items) async {
    return await _toDoDao.insertToDoList(items.map<ToDoTableData>((e) => e.toTable()).toList());
  }

  Future<ToDoItem> getToDoItem(ToDoItem toDoItem) async {
    final res = await _hiveClient.get(box, toDoItem.id);
    return toDoItem.copyWith(id: res.toString());
  }
}
