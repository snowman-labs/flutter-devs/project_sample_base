import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:injectable/injectable.dart';
import 'package:snowtodoapp/data/helpers/resource.dart';
import 'package:snowtodoapp/data/remote/dio_client.dart';
import 'package:snowtodoapp/domain/entities/todo_entity.dart';
import 'package:uuid/uuid.dart';

import 'package:snowtodoapp/data/mappers/todo_item_mapper.dart';

@dev
@injectable
class TodoListRemoteDataSource {
  final DioClient _dioClient;
  final Firestore _firestore;

  TodoListRemoteDataSource(this._dioClient, this._firestore);

  Future<Resource<List<ToDoItem>>> getToDoList() async {
    return Resource.asFuture(() async {
      return await _firestore.collection('todo').getDocuments().then((value) {
        return value.documents.map<ToDoItem>((e) {
          var data = Map<String, dynamic>.from(e.data);
          data['id'] = e.documentID;
          return ToDoItem().fromJson(data);
        }).toList();
      });
    });
  }

  Future<List<ToDoItem>> getPendingToDoItems() async {
    return await _firestore
        .collection('todo')
        .where('isDone', isEqualTo: false)
        .getDocuments()
        .then((value) {
      return value.documents.map<ToDoItem>((e) {
        var data = Map<String, dynamic>.from(e.data);
        data['id'] = e.documentID;
        return ToDoItem().fromJson(data);
      }).toList();
    });
  }

  Future<ToDoItem> setTodoItemAsFinished(Map<String, dynamic> data) async {
    await _firestore.collection('todo').document(data['id']).updateData(data);
    return ToDoItem().fromJson(data);
  }

  Future<ToDoItem> addNewToDoItem(Map<String, dynamic> data) async {
    var id = Uuid().v1();
    await _firestore.collection('todo').document(id).setData(data);
    data['id'] = id;
    return ToDoItem().fromJson(data);
  }
}
