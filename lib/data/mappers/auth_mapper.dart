import 'dart:convert';

import 'package:snowtodoapp/domain/entities/auth_entity.dart';

extension AuthMapper on AuthEntity{

  AuthEntity copyWith({
    String email,
    String password,
  }) {
    return AuthEntity(
      email: email ?? this.email,
      password: password ?? this.password,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'email': email,
      'password': password,
    };
  }

  AuthEntity fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return AuthEntity(
      email: map['email'],
      password: map['password'],
    );
  }

  String toJson() => json.encode(toMap());

  AuthEntity fromJson(String source) => fromMap(json.decode(source));

}