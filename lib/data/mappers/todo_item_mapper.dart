
import 'package:snowtodoapp/data/local/moor_db.dart';
import 'package:snowtodoapp/domain/entities/todo_entity.dart';

extension ToDoItemMapper on ToDoItem {

  Map<String,dynamic> get toJson => {
    ToDoItemFields.iD: id,
    ToDoItemFields.title : title,
    ToDoItemFields.isDone : isDone
  };

  ToDoItem fromJson(Map<dynamic ,dynamic> map) =>
      ToDoItem(
        id: map[ToDoItemFields.iD],
        title: map[ToDoItemFields.title],
        isDone: map[ToDoItemFields.isDone]
      );

  ToDoItem copyWith({
    String id,
    String title,
    bool isDone,
  }) {
    return ToDoItem(
      id: id ?? this.id,
      title: title ?? this.title,
      isDone: isDone ?? this.isDone,
    );
  }

  ToDoTableData toTable(){
    return ToDoTableData(
      uid: id,
      title: title,
      isDone: isDone
    );
  }

  ToDoItem fromTable(ToDoTableData entry){
    return ToDoItem(
      id: entry.uid,
      title: entry.title,
      isDone: entry.isDone,
    );
  }

}

class ToDoItemFields{
  static const String iD = "id";
  static const String title = "title";
  static const String isDone = "isDone";
}

