import 'dart:convert';

import 'package:snowtodoapp/domain/entities/user_entity.dart';

extension UserMapper on UserEntity{

  copyWith({
    String email,
  }) {
    return UserEntity(
      email: email ?? this.email,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'email': email,
    };
  }

  UserEntity fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return UserEntity(
      email: map['email'],
    );
  }

  String toJson() => json.encode(toMap());

  UserEntity fromJson(String source) => fromMap(json.decode(source));

}