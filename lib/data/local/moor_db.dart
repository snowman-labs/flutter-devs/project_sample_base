import 'dart:io';

import 'package:injectable/injectable.dart';
import 'package:moor/moor.dart';
import 'package:moor_ffi/moor_ffi.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:projectbasesnow/config/flavor_config.dart';
import 'package:snowtodoapp/data/local/dao/todo_dao.dart';
import 'package:snowtodoapp/data/model/local/todo_table.dart';

part 'moor_db.g.dart';

LazyDatabase _init() {
  return LazyDatabase(() async {
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path,
        "todo_list_db_${FlavorConfig.name.toLowerCase()}.sqlite"));
    return VmDatabase(file);
  });
}

@singleton
@UseMoor(tables: [ToDoTable], daos: [ToDoDao])
class MoorDB extends _$MoorDB {
  MoorDB() : super(_init());

  @override
  int get schemaVersion => 1;

  @override
  MigrationStrategy get migration => MigrationStrategy(
      onCreate: (m) {
        return m.createAll();
      },
      onUpgrade: (m, from, to) async {});

  Future<void> resetDb() async {
    for (var table in allTables) {
      await delete(table).go();
    }
  }
}
