// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'moor_db.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class ToDoTableData extends DataClass implements Insertable<ToDoTableData> {
  final String uid;
  final String title;
  final bool isDone;
  ToDoTableData(
      {@required this.uid, @required this.title, @required this.isDone});
  factory ToDoTableData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final boolType = db.typeSystem.forDartType<bool>();
    return ToDoTableData(
      uid: stringType.mapFromDatabaseResponse(data['${effectivePrefix}uid']),
      title:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}title']),
      isDone:
          boolType.mapFromDatabaseResponse(data['${effectivePrefix}is_done']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || uid != null) {
      map['uid'] = Variable<String>(uid);
    }
    if (!nullToAbsent || title != null) {
      map['title'] = Variable<String>(title);
    }
    if (!nullToAbsent || isDone != null) {
      map['is_done'] = Variable<bool>(isDone);
    }
    return map;
  }

  ToDoTableCompanion toCompanion(bool nullToAbsent) {
    return ToDoTableCompanion(
      uid: uid == null && nullToAbsent ? const Value.absent() : Value(uid),
      title:
          title == null && nullToAbsent ? const Value.absent() : Value(title),
      isDone:
          isDone == null && nullToAbsent ? const Value.absent() : Value(isDone),
    );
  }

  factory ToDoTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return ToDoTableData(
      uid: serializer.fromJson<String>(json['uid']),
      title: serializer.fromJson<String>(json['title']),
      isDone: serializer.fromJson<bool>(json['isDone']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uid': serializer.toJson<String>(uid),
      'title': serializer.toJson<String>(title),
      'isDone': serializer.toJson<bool>(isDone),
    };
  }

  ToDoTableData copyWith({String uid, String title, bool isDone}) =>
      ToDoTableData(
        uid: uid ?? this.uid,
        title: title ?? this.title,
        isDone: isDone ?? this.isDone,
      );
  @override
  String toString() {
    return (StringBuffer('ToDoTableData(')
          ..write('uid: $uid, ')
          ..write('title: $title, ')
          ..write('isDone: $isDone')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(uid.hashCode, $mrjc(title.hashCode, isDone.hashCode)));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is ToDoTableData &&
          other.uid == this.uid &&
          other.title == this.title &&
          other.isDone == this.isDone);
}

class ToDoTableCompanion extends UpdateCompanion<ToDoTableData> {
  final Value<String> uid;
  final Value<String> title;
  final Value<bool> isDone;
  const ToDoTableCompanion({
    this.uid = const Value.absent(),
    this.title = const Value.absent(),
    this.isDone = const Value.absent(),
  });
  ToDoTableCompanion.insert({
    @required String uid,
    @required String title,
    @required bool isDone,
  })  : uid = Value(uid),
        title = Value(title),
        isDone = Value(isDone);
  static Insertable<ToDoTableData> custom({
    Expression<String> uid,
    Expression<String> title,
    Expression<bool> isDone,
  }) {
    return RawValuesInsertable({
      if (uid != null) 'uid': uid,
      if (title != null) 'title': title,
      if (isDone != null) 'is_done': isDone,
    });
  }

  ToDoTableCompanion copyWith(
      {Value<String> uid, Value<String> title, Value<bool> isDone}) {
    return ToDoTableCompanion(
      uid: uid ?? this.uid,
      title: title ?? this.title,
      isDone: isDone ?? this.isDone,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uid.present) {
      map['uid'] = Variable<String>(uid.value);
    }
    if (title.present) {
      map['title'] = Variable<String>(title.value);
    }
    if (isDone.present) {
      map['is_done'] = Variable<bool>(isDone.value);
    }
    return map;
  }
}

class $ToDoTableTable extends ToDoTable
    with TableInfo<$ToDoTableTable, ToDoTableData> {
  final GeneratedDatabase _db;
  final String _alias;
  $ToDoTableTable(this._db, [this._alias]);
  final VerificationMeta _uidMeta = const VerificationMeta('uid');
  GeneratedTextColumn _uid;
  @override
  GeneratedTextColumn get uid => _uid ??= _constructUid();
  GeneratedTextColumn _constructUid() {
    return GeneratedTextColumn(
      'uid',
      $tableName,
      false,
    );
  }

  final VerificationMeta _titleMeta = const VerificationMeta('title');
  GeneratedTextColumn _title;
  @override
  GeneratedTextColumn get title => _title ??= _constructTitle();
  GeneratedTextColumn _constructTitle() {
    return GeneratedTextColumn(
      'title',
      $tableName,
      false,
    );
  }

  final VerificationMeta _isDoneMeta = const VerificationMeta('isDone');
  GeneratedBoolColumn _isDone;
  @override
  GeneratedBoolColumn get isDone => _isDone ??= _constructIsDone();
  GeneratedBoolColumn _constructIsDone() {
    return GeneratedBoolColumn(
      'is_done',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [uid, title, isDone];
  @override
  $ToDoTableTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'to_do_table';
  @override
  final String actualTableName = 'to_do_table';
  @override
  VerificationContext validateIntegrity(Insertable<ToDoTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('uid')) {
      context.handle(
          _uidMeta, uid.isAcceptableOrUnknown(data['uid'], _uidMeta));
    } else if (isInserting) {
      context.missing(_uidMeta);
    }
    if (data.containsKey('title')) {
      context.handle(
          _titleMeta, title.isAcceptableOrUnknown(data['title'], _titleMeta));
    } else if (isInserting) {
      context.missing(_titleMeta);
    }
    if (data.containsKey('is_done')) {
      context.handle(_isDoneMeta,
          isDone.isAcceptableOrUnknown(data['is_done'], _isDoneMeta));
    } else if (isInserting) {
      context.missing(_isDoneMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {uid};
  @override
  ToDoTableData map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return ToDoTableData.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $ToDoTableTable createAlias(String alias) {
    return $ToDoTableTable(_db, alias);
  }
}

abstract class _$MoorDB extends GeneratedDatabase {
  _$MoorDB(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $ToDoTableTable _toDoTable;
  $ToDoTableTable get toDoTable => _toDoTable ??= $ToDoTableTable(this);
  ToDoDao _toDoDao;
  ToDoDao get toDoDao => _toDoDao ??= ToDoDao(this as MoorDB);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [toDoTable];
}
