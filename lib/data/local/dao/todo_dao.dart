import 'package:injectable/injectable.dart';
import 'package:moor/moor.dart';
import 'package:snowtodoapp/data/local/moor_db.dart';
import 'package:snowtodoapp/data/model/local/todo_table.dart';

part 'todo_dao.g.dart';

@injectable
@UseDao(tables: [ToDoTable])
class ToDoDao extends DatabaseAccessor<MoorDB> with _$ToDoDaoMixin {
  ToDoDao(MoorDB db) : super(db);

  Stream<List<ToDoTableData>> get allActiveToDoItemsStream =>
      select(toDoTable).watch();

  Future<List<ToDoTableData>> getAllToDoItems() => select(toDoTable).get();

  Future<int> insertToDo(ToDoTableData entry) {
    return into(toDoTable).insert(entry, mode: InsertMode.insertOrReplace);
  }

  Future<void> insertToDoList(List<ToDoTableData> entries) async {
    return await batch((batch) =>
        batch.insertAll(toDoTable, entries, mode: InsertMode.insertOrReplace));
  }

  Future<bool> updateToDo(ToDoTableData entry) {
    return (update(toDoTable).replace(entry)).then((value) {
      print(value ? "Update goal row success" : "Update goal row failed");
      return value;
    });
  }

  Future<int> deleteToDoById(String id) =>
      (delete(toDoTable)..where((g) => g.uid.equals(id))).go().then((value) {
        print("Row affecteds: $value");
        return value;
      });
}
