// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'todo_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$ToDoDaoMixin on DatabaseAccessor<MoorDB> {
  $ToDoTableTable get toDoTable => attachedDatabase.toDoTable;
}
