import 'package:snowtodoapp/domain/entities/user_entity.dart';

abstract class AuthRepository {
  Future<UserEntity> getUser();
  Future<UserEntity> loginUserEmail(String email, String password);
  Future<UserEntity> registerUserEmail(String email, String password);
  Future<void> logoutUser();
  Stream<UserEntity> getUserStream();
}
