import 'package:snowtodoapp/data/helpers/resource.dart';
import 'package:snowtodoapp/domain/entities/todo_entity.dart';

abstract class TodoListRepository{

  Stream<Resource<List<ToDoItem>>> toDoItemsStream();
  Future<Resource<List<ToDoItem>>> getToDoList();
  Future<Resource<ToDoItem>> setTodoItemAsFinished(ToDoItem toDoItem);
  Future<Resource<ToDoItem>> addNewToDoItem(ToDoItem todoItem);

}