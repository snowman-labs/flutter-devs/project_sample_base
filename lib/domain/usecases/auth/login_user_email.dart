import 'package:injectable/injectable.dart';
import 'package:snowtodoapp/domain/entities/auth_entity.dart';
import 'package:snowtodoapp/domain/entities/user_entity.dart';
import 'package:snowtodoapp/domain/repositories/auth/auth_repository.dart';
import 'package:snowtodoapp/domain/usecases/base/base_future_use_case.dart';

@injectable
class LoginUserEmail extends BaseFutureUseCase<AuthEntity, UserEntity> {
  final AuthRepository _repository;

  LoginUserEmail(this._repository);

  @override
  Future<UserEntity> call([AuthEntity params]) => _repository.loginUserEmail(params.email, params.password);
}