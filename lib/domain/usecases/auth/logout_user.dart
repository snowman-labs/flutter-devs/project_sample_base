import 'package:injectable/injectable.dart';
import 'package:snowtodoapp/domain/repositories/auth/auth_repository.dart';
import 'package:snowtodoapp/domain/usecases/base/base_future_use_case.dart';

@injectable
class LogoutUser extends BaseFutureUseCase<void, void> {
  final AuthRepository _repository;

  LogoutUser(this._repository);

  @override
  Future<void> call([void params]) => _repository.logoutUser();
}