import 'package:injectable/injectable.dart';
import 'package:snowtodoapp/domain/entities/auth_entity.dart';
import 'package:snowtodoapp/domain/entities/user_entity.dart';
import 'package:snowtodoapp/domain/repositories/auth/auth_repository.dart';
import 'package:snowtodoapp/domain/usecases/base/base_future_use_case.dart';

@injectable
class RegisterUserEmail extends BaseFutureUseCase<AuthEntity, UserEntity> {
  final AuthRepository _repository;

  RegisterUserEmail(this._repository);

  @override
  Future<UserEntity> call([AuthEntity params]) => _repository.registerUserEmail(params.email, params.password);
}