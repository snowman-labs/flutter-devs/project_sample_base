import 'dart:async';

import 'package:injectable/injectable.dart';
import 'package:snowtodoapp/domain/entities/auth_status.dart';
import 'package:snowtodoapp/domain/entities/user_entity.dart';
import 'package:snowtodoapp/domain/repositories/auth/auth_repository.dart';
import 'package:snowtodoapp/domain/usecases/base/base_stream_use_case.dart';

@singleton
class GetAuthStatusStream extends BaseStreamUseCase<void, AuthStatus> {
  final AuthRepository _repository;

  GetAuthStatusStream(this._repository) {
    stream = _repository.getUserStream().transform(
      StreamTransformer<UserEntity, AuthStatus>.fromHandlers(
        handleData: (user, sink) {
          sink.add(user == null ? AuthStatus.logoff : AuthStatus.login);
        },
      ),
    );
  }

  Stream<AuthStatus> stream;

  @override
  Stream<AuthStatus> call([void params]) {
    return stream;
  }
}
