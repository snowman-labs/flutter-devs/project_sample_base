import 'package:injectable/injectable.dart';
import 'package:snowtodoapp/domain/entities/user_entity.dart';
import 'package:snowtodoapp/domain/repositories/auth/auth_repository.dart';
import 'package:snowtodoapp/domain/usecases/base/base_stream_use_case.dart';

@injectable
class GetUserStream extends BaseStreamUseCase<void, UserEntity> {
  final AuthRepository _repository;

  GetUserStream(this._repository);

  @override
  Stream<UserEntity> call([void params]) => _repository.getUserStream();
}
