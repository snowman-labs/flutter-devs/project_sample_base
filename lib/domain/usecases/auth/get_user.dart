import 'package:injectable/injectable.dart';
import 'package:snowtodoapp/domain/entities/user_entity.dart';
import 'package:snowtodoapp/domain/repositories/auth/auth_repository.dart';
import 'package:snowtodoapp/domain/usecases/base/base_future_use_case.dart';

@injectable
class GetUser extends BaseFutureUseCase<void, UserEntity> {
  final AuthRepository _repository;

  GetUser(this._repository);

  @override
  Future<UserEntity> call([void params]) => _repository.getUser();
}