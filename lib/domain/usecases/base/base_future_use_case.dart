import 'package:snowtodoapp/domain/usecases/base/base_use_case.dart';

abstract class BaseFutureUseCase<Params, Result> extends BaseUseCase {
  Future<Result> call([Params params]);
}
