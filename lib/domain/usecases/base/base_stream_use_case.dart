import 'package:snowtodoapp/domain/usecases/base/base_use_case.dart';

abstract class BaseStreamUseCase<R, T> extends BaseUseCase {
  Stream<T> call([R params]);
}
