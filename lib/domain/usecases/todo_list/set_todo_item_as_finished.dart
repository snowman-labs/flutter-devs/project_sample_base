import 'package:injectable/injectable.dart';
import 'package:snowtodoapp/data/helpers/resource.dart';
import 'package:snowtodoapp/domain/entities/todo_entity.dart';
import 'package:snowtodoapp/domain/repositories/todo_list/todo_list_repository.dart';

import '../base/base_future_use_case.dart';

@injectable
class SetToDoItemAsFinished implements BaseFutureUseCase<ToDoItem, Resource<ToDoItem>>{

  final TodoListRepository _repository;

  SetToDoItemAsFinished(this._repository);

  @override
  Future<Resource<ToDoItem>> call([ToDoItem params]) => _repository.setTodoItemAsFinished(params);

}