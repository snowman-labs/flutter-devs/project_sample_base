import 'package:injectable/injectable.dart';
import 'package:snowtodoapp/data/helpers/resource.dart';
import 'package:snowtodoapp/domain/entities/todo_entity.dart';
import 'package:snowtodoapp/domain/repositories/todo_list/todo_list_repository.dart';
import 'package:snowtodoapp/domain/usecases/base/base_stream_use_case.dart';

@injectable
class GetToDoListStream extends BaseStreamUseCase<void,Resource<List<ToDoItem>>>{

  final TodoListRepository _repository;

  GetToDoListStream(this._repository);

  @override
  Stream<Resource<List<ToDoItem>>> call([void params]) => _repository.toDoItemsStream();

}