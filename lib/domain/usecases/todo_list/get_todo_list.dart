
import 'package:injectable/injectable.dart';
import 'package:snowtodoapp/data/helpers/resource.dart';
import 'package:snowtodoapp/domain/entities/todo_entity.dart';
import 'package:snowtodoapp/domain/repositories/todo_list/todo_list_repository.dart';

import '../base/base_future_use_case.dart';

@injectable
class GetTodoList implements BaseFutureUseCase<void, Resource<List<ToDoItem>>>{

  final TodoListRepository _repository;

  GetTodoList(this._repository);

  @override
  Future<Resource<List<ToDoItem>>> call([void params]) => _repository.getToDoList();

}