import 'package:flutter/foundation.dart';

@immutable
class UserEntity {

  final String email;

  UserEntity({
    this.email,
  });

  @override
  String toString() => 'UserModel(email: $email)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is UserEntity &&
      o.email == email;
  }

  @override
  int get hashCode => email.hashCode;
}
