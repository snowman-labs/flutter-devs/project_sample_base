import 'package:flutter/foundation.dart';

@immutable
class AuthEntity {
  final String email;
  final String password;

  AuthEntity({
    this.email,
    this.password,
  });

  @override
  String toString() => 'AuthInfo(email: $email, password: $password)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is AuthEntity && o.email == email && o.password == password;
  }

  @override
  int get hashCode => email.hashCode ^ password.hashCode;
}
