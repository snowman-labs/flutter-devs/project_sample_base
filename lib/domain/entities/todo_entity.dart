
import 'package:equatable/equatable.dart';

class ToDoItem extends Equatable {
  final String id;
  final String title;
  final bool isDone;

  ToDoItem({this.id, this.title, this.isDone = false});

  @override
  List<Object> get props => [id, title, isDone];
}
