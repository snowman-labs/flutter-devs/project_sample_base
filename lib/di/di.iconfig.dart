// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:snowtodoapp/data/remote/dio_client.dart';
import 'package:snowtodoapp/di/modules/remote_module.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:snowtodoapp/di/modules/local_module.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:snowtodoapp/data/local/hive_client.dart';
import 'package:snowtodoapp/data/local/moor_db.dart';
import 'package:snowtodoapp/data/local/dao/todo_dao.dart';
import 'package:snowtodoapp/data/data_sources/todo_list/todo_list_local_data_source.dart';
import 'package:snowtodoapp/data/data_sources/todo_list/todo_list_remote_data_source.dart';
import 'package:snowtodoapp/data/repositories/todo_list/todo_list_repository_impl.dart';
import 'package:snowtodoapp/domain/repositories/todo_list/todo_list_repository.dart';
import 'package:snowtodoapp/domain/usecases/todo_list/add_new_todo_item.dart';
import 'package:snowtodoapp/data/data_sources/auth/auth_local_data_source.dart';
import 'package:snowtodoapp/data/data_sources/auth/auth_remote_data_source.dart';
import 'package:snowtodoapp/data/repositories/auth/auth_repository_impl.dart';
import 'package:snowtodoapp/domain/repositories/auth/auth_repository.dart';
import 'package:snowtodoapp/domain/usecases/auth/get_auth_status_stream.dart';
import 'package:snowtodoapp/domain/usecases/todo_list/get_todo_list_stream.dart';
import 'package:snowtodoapp/domain/usecases/todo_list/get_todo_list.dart';
import 'package:snowtodoapp/domain/usecases/auth/get_user.dart';
import 'package:snowtodoapp/domain/usecases/auth/get_user_stream.dart';
import 'package:snowtodoapp/domain/usecases/auth/login_user_email.dart';
import 'package:snowtodoapp/domain/usecases/auth/logout_user.dart';
import 'package:snowtodoapp/domain/usecases/auth/register_user_email.dart';
import 'package:snowtodoapp/domain/usecases/todo_list/set_todo_item_as_finished.dart';
import 'package:get_it/get_it.dart';

void $initGetIt(GetIt g, {String environment}) {
  final remoteModule = _$RemoteModule();
  final localModule = _$LocalModule();
  g.registerFactory<FirebaseAuth>(() => remoteModule.firebaseAuth);
  g.registerFactory<Firestore>(() => remoteModule.firestore);
  g.registerFactory<ToDoDao>(() => ToDoDao(g<MoorDB>()));
  g.registerFactory<TodoListLocalDataSource>(
      () => TodoListLocalDataSource(g<HiveClient>(), g<ToDoDao>()));
  g.registerFactory<TodoListRepository>(() => TodoListRepositoryImpl(
      g<TodoListRemoteDataSource>(), g<TodoListLocalDataSource>()));
  g.registerFactory<AddNewToDoItem>(
      () => AddNewToDoItem(g<TodoListRepository>()));
  g.registerFactory<AuthLocalDataSource>(
      () => AuthLocalDataSource(g<FlutterSecureStorage>(), g<MoorDB>()));
  g.registerFactory<AuthRemoteDataSource>(
      () => AuthRemoteDataSource(g<FirebaseAuth>()));
  g.registerFactory<GetToDoListStream>(
      () => GetToDoListStream(g<TodoListRepository>()));
  g.registerFactory<GetTodoList>(() => GetTodoList(g<TodoListRepository>()));
  g.registerFactory<GetUser>(() => GetUser(g<AuthRepository>()));
  g.registerFactory<GetUserStream>(() => GetUserStream(g<AuthRepository>()));
  g.registerFactory<LoginUserEmail>(() => LoginUserEmail(g<AuthRepository>()));
  g.registerFactory<LogoutUser>(() => LogoutUser(g<AuthRepository>()));
  g.registerFactory<RegisterUserEmail>(
      () => RegisterUserEmail(g<AuthRepository>()));
  g.registerFactory<SetToDoItemAsFinished>(
      () => SetToDoItemAsFinished(g<TodoListRepository>()));

  //Register dev Dependencies --------
  if (environment == 'dev') {
    g.registerFactory<TodoListRemoteDataSource>(
        () => TodoListRemoteDataSource(g<DioClient>(), g<Firestore>()));
  }

  //Eager singletons must be registered in the right order
  g.registerSingleton<DioClient>(DioClient());
  g.registerSingleton<FlutterSecureStorage>(localModule.secureStorage);
  g.registerSingleton<HiveClient>(HiveClient());
  g.registerSingleton<MoorDB>(MoorDB());
  g.registerSingleton<AuthRepository>(
      AuthRepositoryImpl(g<AuthRemoteDataSource>(), g<AuthLocalDataSource>()));
  g.registerSingleton<GetAuthStatusStream>(
      GetAuthStatusStream(g<AuthRepository>()));
}

class _$RemoteModule extends RemoteModule {}

class _$LocalModule extends LocalModule {}
