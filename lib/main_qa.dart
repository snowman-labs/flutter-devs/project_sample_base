import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:projectbasesnow/projectbasesnow.dart';
import 'package:snowtodoapp/data/constants/constants.dart';

import 'app/app_module.dart';
import 'di/di.dart';
import 'flavor_values.dart';

void main() {
  RunAppSnow(
    ModularApp(
      module: AppModule(),
    ),
    flavorValues: Constants.flavorQa,
    getItInit: () => configureInjection(qa.name),
    flavor: Flavor.qa,
    errorReporter: Crashlytics.instance.recordFlutterError,
  );
}
