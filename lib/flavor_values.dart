import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:projectbasesnow/projectbasesnow.dart';

class FlavorValuesApp implements FlavorValues {
  const FlavorValuesApp({
    @required this.baseUrl,
    this.anotherUrl,
    @required Map<String, bool> Function() features,
  }) : _features = features;

  final String baseUrl;
  final String anotherUrl;
  final Map<String, bool> Function() _features;
  Map<String, bool> get features => _features();
  //Add other flavor specific values, e.g database name
}

const qa = Environment('qa');
