import 'dart:async';

import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:snowtodoapp/di/di.dart';
import 'package:snowtodoapp/domain/entities/auth_status.dart';
import 'package:snowtodoapp/domain/usecases/auth/get_auth_status_stream.dart';
part 'auth_store.g.dart';

class AuthStore = _AuthStoreBase with _$AuthStore;

abstract class _AuthStoreBase extends Disposable with Store {
  final authStatusStream = getIt<GetAuthStatusStream>();

  StreamSubscription subscription;

  _AuthStoreBase() {
    subscription = authStatusStream().listen(_authListener);
  }

  void _authListener(AuthStatus status) {
    if (status == AuthStatus.login) {
      Modular.to.popUntil((route) => route.isFirst);
      Modular.to.pushReplacementNamed('/home');
    } else if (status == AuthStatus.logoff) {
      Modular.to.popUntil((route) => route.isFirst);
      Modular.to.pushReplacementNamed('/login');
    }
  }

  @override
  void dispose() { 
    subscription.cancel();
  }
}