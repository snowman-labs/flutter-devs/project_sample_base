import 'package:i18n_extension/i18n_extension.dart';

// For more info, see: https://pub.dartlang.org/packages/i18n_extension

extension HomeLocalization on String {
  static var t = Translations("pt_br") + 
      {
        "pt_br": "Lista de afazeres",
        "en_us": "To do list",
      } +
      {
        "pt_br": "Minhas tarefas",
        "en_us": "My Tasks",
      } +
      {
        "pt_br": "Sair",
        "en_us": "Logout",
      } +
      {
        "pt_br": "Sua lista de afazeres está vazia",
        "en_us": "Your to do list is empty",
      } +
      {
        "pt_br": "Nome da tarefa",
        "en_us": "Task name",
      }
      ;

  String get i18n => localize(this, t);

  String fill(List<Object> params) => localizeFill(this, params);

  String plural(int value) => localizePlural(value, this, t);

  String version(Object modifier) => localizeVersion(modifier, this, t);

  Map<String, String> allVersions() => localizeAllVersions(this, t);
}
