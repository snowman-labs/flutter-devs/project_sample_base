import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:projectbasesnow/projectbasesnow.dart';
import 'package:snowtodoapp/app/styles/app_theme_data.dart';

class AppWidget extends StatelessWidget {
  final FirebaseAnalytics analytics = FirebaseAnalytics();
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return SnowMaterialApp(
      title: 'Flutter Demo',
      theme: AppThemeData.themeDataLight,
      darkTheme: AppThemeData.themeDataDark,
      initialRoute: '/',
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
    );
  }
}
