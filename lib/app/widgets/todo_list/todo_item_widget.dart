import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:snowtodoapp/domain/entities/todo_entity.dart';

class ToDoItemWidget extends StatelessWidget {
  final ToDoItem toDoItem;
  final void Function(ToDoItem) onDone;

  const ToDoItemWidget({Key key, this.toDoItem, this.onDone}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 7.0),
      color: Theme.of(context).accentColor,
      height: 85.0,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                color: Colors.blueAccent,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10.0),
                    bottomLeft: Radius.circular(10.0))),
            width: 4.0,
          ),
          SizedBox(
            width: 10.0,
          ),
          Expanded(
            child: Text(
              toDoItem.title,
              style: GoogleFonts.workSans(
                fontSize: 18.0,
              ),
            ),
          ),
          IconButton(
            icon: toDoItem.isDone
                ? Icon(FontAwesomeIcons.solidCheckSquare)
                : Icon(FontAwesomeIcons.square),
            onPressed: () {
              onDone(toDoItem);
            },
          )
        ],
      ),
    );
  }
}
