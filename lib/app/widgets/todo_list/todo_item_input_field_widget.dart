import 'package:flutter/material.dart';

class TodoItemInputField extends StatelessWidget {
  final Function onChanged;
  final String hintText;

  const TodoItemInputField({@required this.onChanged, this.hintText = ""});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(20.0)),
      child: TextField(
        textCapitalization: TextCapitalization.words,
        cursorColor: Colors.white,
        textInputAction: TextInputAction.done,
        onChanged: onChanged,
        decoration: InputDecoration(
          hintText: hintText,
          border: InputBorder.none,
          hintStyle: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
