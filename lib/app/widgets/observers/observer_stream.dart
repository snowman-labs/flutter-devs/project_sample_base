

import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:mobx_widget/mobx_widget.dart' as mobx;
import 'package:snowtodoapp/data/helpers/resource.dart';

class ObserverStream<T> extends StatelessWidget {

  final ObservableStream<Resource> Function() observableStream;
  final Widget Function(BuildContext context, Exception error) onError;
  final Widget Function(BuildContext context) onPending;
  final Widget Function(BuildContext context) onUnstarted;
  final Widget Function(BuildContext context) onNull;

  final Widget Function(BuildContext context, Resource data) onData;

  const ObserverStream({
    Key key,
    @required this.observableStream,
    @required this.onData,
    this.onNull,
    this.onPending,
    this.onError,
    this.onUnstarted,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return mobx.ObserverStream<Resource, Exception>(
      key: key,
      observableStream: observableStream,
      onData: onData,
      onError: onError,
      onNull: onNull,
      onUnstarted: onUnstarted,
    );
  }
}

