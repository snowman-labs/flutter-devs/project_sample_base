import 'package:flutter/material.dart';
import 'package:snowtodoapp/app/utils/validators.dart';

class EmailtextFieldWidget extends StatelessWidget {
  final TextEditingController controller;
  final void Function(String) onSaved;
  final String label;

  const EmailtextFieldWidget({
    Key key,
    this.controller,
    this.onSaved,
    this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      onSaved: onSaved,
      validator: Validators.email,
      decoration: InputDecoration(labelText: label),
    );
  }
}
