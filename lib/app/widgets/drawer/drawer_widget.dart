import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:snowtodoapp/app/i18n/drawer_widget_i18n.dart';
import 'package:snowtodoapp/data/constants/constants.dart';
import 'package:snowtodoapp/di/di.dart';
import 'package:snowtodoapp/domain/usecases/auth/logout_user.dart';

class DrawerWidget extends StatelessWidget {
  const DrawerWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
          children: [
            UserAccountsDrawerHeader(
                accountName: Text(""), accountEmail: Text("")),
            if(Constants.flavor.features['feature_1']) ListTile(
              title: Text("Feature 1".i18n),
              key: Key("feature_1"),
              onTap: () => Modular.to.pushNamed('/home/feature1'),
            ),
            if(Constants.flavor.features['feature_2']) ListTile(
              title: Text("Feature 2".i18n),
              key: Key("feature_2"),
              onTap: () => Modular.to.pushNamed('/home/feature2'),
            ),
            ListTile(
              title: Text("Sair".i18n),
              key: Key("logout"),
              onTap: getIt.get<LogoutUser>(),
            ),
          ],
        ),
      );
  }
}