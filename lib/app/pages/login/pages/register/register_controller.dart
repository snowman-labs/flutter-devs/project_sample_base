import 'package:mobx/mobx.dart';
import 'package:snowtodoapp/di/di.dart';
import 'package:snowtodoapp/domain/entities/auth_entity.dart';
import 'package:snowtodoapp/domain/entities/user_entity.dart';
import 'package:snowtodoapp/domain/usecases/auth/register_user_email.dart';

part 'register_controller.g.dart';

class RegisterController = _RegisterControllerBase with _$RegisterController;

abstract class _RegisterControllerBase with Store {
  final registerUserEmail = getIt.get<RegisterUserEmail>();

  @observable
  bool loading = false;

  @action
  Future<bool> registerWithEmail(String email, String password) async {
    UserEntity user;
    try {
      loading = true;
      user = await registerUserEmail(AuthEntity(email: email, password: password));
    } finally {
      loading = false;
    }
    return user != null;
  }
}
