import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:snowtodoapp/app/i18n/login_i18n.dart';
import 'package:snowtodoapp/app/widgets/custom_alert_dialog/custom_alert_dialog.dart';
import 'package:snowtodoapp/app/widgets/login/email_text_field_widget.dart';
import 'package:snowtodoapp/app/widgets/login/password_text_field_widget.dart';

import 'register_controller.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState
    extends ModularState<RegisterPage, RegisterController> {
  //use 'controller' variable to access controller
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  Future<void> register() async {
    if (_formKey.currentState.validate()) {
      if (!await controller.registerWithEmail(
        _emailController.text,
        _passwordController.text,
      )) {
        CustomAlertDialog.error(context, "Erro no cadastro");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cadastro".i18n),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              EmailtextFieldWidget(
                controller: _emailController,
                label: "E-mail".i18n,
              ),
              PasswordTextFieldWidget(
                controller: _passwordController,
                label: "Senha".i18n,
              ),
              SizedBox(
                height: 25,
              ),
              Observer(builder: (_) {
                return RaisedButton(
                  onPressed: controller.loading ? null : register,
                  child: Text("Cadastrar-se".i18n),
                );
              }),
            ],
          ),
        ),
      ),
    );
  }
}
