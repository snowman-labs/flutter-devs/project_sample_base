import 'package:mobx/mobx.dart';
import 'package:snowtodoapp/di/di.dart';
import 'package:snowtodoapp/domain/entities/auth_entity.dart';
import 'package:snowtodoapp/domain/entities/user_entity.dart';
import 'package:snowtodoapp/domain/usecases/auth/login_user_email.dart';

part 'login_controller.g.dart';

class LoginController = _LoginBase with _$LoginController;

abstract class _LoginBase with Store {
  final loginUserEmail = getIt.get<LoginUserEmail>();

  @observable
  bool loading = false;
  
  @action
  Future<bool> loginWithEmail(String email, String password) async {
    UserEntity user;
    try {
      loading = true;
      user = await loginUserEmail(AuthEntity(email: email, password: password));
    } catch (e) {} finally {
      loading = false;
    }
    return user != null;
  }
}
