import 'package:flutter_modular/flutter_modular.dart';

import 'pages/login/login_controller.dart';
import 'pages/login/login_page.dart';
import 'pages/register/register_controller.dart';
import 'pages/register/register_page.dart';

class LoginModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => RegisterController()),
        Bind((i) => LoginController()),
      ];

  @override
  List<Router> get routers => [
        Router('/', child: (_, args) => LoginPage()),
        Router('/register', child: (_, args) => RegisterPage(), transition: TransitionType.defaultTransition),
      ];

  static Inject get to => Inject<LoginModule>.of();
}
