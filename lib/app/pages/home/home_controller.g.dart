// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeController on _HomeControllerBase, Store {
  final _$todoListAtom = Atom(name: '_HomeControllerBase.todoList');

  @override
  ObservableStream<Resource<List<ToDoItem>>> get todoList {
    _$todoListAtom.reportRead();
    return super.todoList;
  }

  @override
  set todoList(ObservableStream<Resource<List<ToDoItem>>> value) {
    _$todoListAtom.reportWrite(value, super.todoList, () {
      super.todoList = value;
    });
  }

  final _$showingInputFieldAtom =
      Atom(name: '_HomeControllerBase.showingInputField');

  @override
  bool get showingInputField {
    _$showingInputFieldAtom.reportRead();
    return super.showingInputField;
  }

  @override
  set showingInputField(bool value) {
    _$showingInputFieldAtom.reportWrite(value, super.showingInputField, () {
      super.showingInputField = value;
    });
  }

  final _$saveNewTodoItemAsyncAction =
      AsyncAction('_HomeControllerBase.saveNewTodoItem');

  @override
  Future<void> saveNewTodoItem() {
    return _$saveNewTodoItemAsyncAction.run(() => super.saveNewTodoItem());
  }

  final _$setTodoItemAsFinishedAsyncAction =
      AsyncAction('_HomeControllerBase.setTodoItemAsFinished');

  @override
  Future<void> setTodoItemAsFinished(ToDoItem toDoItem) {
    return _$setTodoItemAsFinishedAsyncAction
        .run(() => super.setTodoItemAsFinished(toDoItem));
  }

  final _$_HomeControllerBaseActionController =
      ActionController(name: '_HomeControllerBase');

  @override
  void newTodoItem() {
    final _$actionInfo = _$_HomeControllerBaseActionController.startAction(
        name: '_HomeControllerBase.newTodoItem');
    try {
      return super.newTodoItem();
    } finally {
      _$_HomeControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
todoList: ${todoList},
showingInputField: ${showingInputField}
    ''';
  }
}
