import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:snowtodoapp/app/i18n/home_i18n.dart';
import 'package:snowtodoapp/app/pages/home/home_controller.dart';
import 'package:snowtodoapp/app/widgets/drawer/drawer_widget.dart';
import 'package:snowtodoapp/app/widgets/todo_list/todo_item_input_field_widget.dart';
import 'package:snowtodoapp/app/widgets/todo_list/todo_item_widget.dart';
import 'package:snowtodoapp/data/constants/status.dart';
import 'package:snowtodoapp/domain/entities/todo_entity.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      drawer: DrawerWidget(),
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: true,
        title: Text(
          "Lista de afazeres".i18n,
          style: GoogleFonts.workSans(),
        ),
        actions: <Widget>[
          IconButton(
            icon: Observer(
              builder: (_) {
                return Icon(
                  controller.showingInputField
                      ? FontAwesomeIcons.check
                      : FontAwesomeIcons.plus,
                  size: 20.0,
                );
              },
            ),
            onPressed: () {
              controller.showingInputField
                  ? controller.saveNewTodoItem()
                  : controller.newTodoItem();
            },
          )
        ],
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.only(top: 16.0, left: 20.0, bottom: 16.0),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.only(topRight: Radius.circular(90.0)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 16.0, right: 35.0, bottom: 16.0),
                child: Observer(
                  builder: (_) {
                    return controller.showingInputField
                        ? TodoItemInputField(
                            onChanged: controller.onChanged,
                            hintText: "Nome da tarefa".i18n,
                          )
                        : Container(
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Minhas tarefas".i18n,
                                style: GoogleFonts.workSans(
                                  fontSize: 25.0,
                                ),
                              ),
                            ),
                          );
                  },
                ),
              ),
              Expanded(
                child: Observer(
                  builder: (_){
                    final todoList = controller.todoList.data;
                    switch(todoList?.status){
                      case Status.loading:
                        return todoList.data == null ?
                        Center(
                          child: CircularProgressIndicator(),
                        ) : _buildList(todoList.data);
                      case Status.success:
                        return _buildList(todoList.data);
                      case Status.failed:
                        return Center(
                          child: Text(todoList.error.toString()),
                        );
                      default:
                        return Container();
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildList(List<ToDoItem> items){
    return items.isNotEmpty
        ? ListView.builder(
      itemCount: items.length,
      itemBuilder: (context, index) {
        return ToDoItemWidget(
          toDoItem: items[index],
          onDone: controller.setTodoItemAsFinished,
        );
      },
    ) : Center(
      child: Text(
        "Sua lista de afazeres está vazia".i18n,
        style: GoogleFonts.workSans(fontSize: 18.0),
      ),
    );
  }
}
