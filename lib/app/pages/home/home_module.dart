import 'package:flutter_modular/flutter_modular.dart';
import 'package:snowtodoapp/app/pages/feature1/feature1_page.dart';
import 'package:snowtodoapp/app/pages/feature2/feature2_page.dart';
import 'package:snowtodoapp/app/pages/home/home_controller.dart';
import 'package:snowtodoapp/app/pages/home/home_page.dart';
import 'package:snowtodoapp/data/constants/constants.dart';

class HomeModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => HomeController()),
      ];

  @override
  List<Router> get routers => [
        Router(
          Modular.initialRoute,
          child: (_, args) => HomePage(),
        ),
        if(Constants.flavor.features['feature_1']) Router(
          '/feature1',
          child: (_, args) => Feature1Page(),
        ),
        if(Constants.flavor.features['feature_2']) Router(
          '/feature2',
          child: (_, args) => Feature2Page(),
        ),
      ];

  static Inject get to => Inject<HomeModule>.of();
}
