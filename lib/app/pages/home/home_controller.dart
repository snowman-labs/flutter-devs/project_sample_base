import 'dart:async';

import 'package:mobx/mobx.dart';
import 'package:snowtodoapp/data/helpers/resource.dart';
import 'package:snowtodoapp/di/di.dart';
import 'package:snowtodoapp/domain/entities/todo_entity.dart';
import 'package:snowtodoapp/domain/usecases/todo_list/add_new_todo_item.dart';
import 'package:snowtodoapp/domain/usecases/todo_list/get_todo_list_stream.dart';
import 'package:snowtodoapp/domain/usecases/todo_list/set_todo_item_as_finished.dart';

part 'home_controller.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  final _setTodoItemAsFinishedUseCase = getIt<SetToDoItemAsFinished>();
  final _addNewToDoItemUseCase = getIt<AddNewToDoItem>();
  final _getToDoListStreamUseCase = getIt<GetToDoListStream>();

  _HomeControllerBase() {
    todoList = _getToDoListStreamUseCase.call().asObservable(initialValue: Resource.loading());
  }

  @observable
  ObservableStream<Resource<List<ToDoItem>>> todoList;

  var _todoItemTitle = "";

  @action
  Future<void> saveNewTodoItem() async {
    showingInputField = false;

    if (_todoItemTitle.isNotEmpty) {
      await _addNewToDoItemUseCase(ToDoItem(
        title: _todoItemTitle,
        isDone: false,
      ));
    }
  }

  @action
  Future<void> setTodoItemAsFinished(ToDoItem toDoItem) async {
    await _setTodoItemAsFinishedUseCase(toDoItem);
  }

  void onChanged(String value) {
    _todoItemTitle = value;
  }

  @action
  void newTodoItem() => showingInputField = true;

  @observable
  var showingInputField = false;
}
