import 'package:flutter/material.dart';

class Feature1Page extends StatefulWidget {
  final String title;
  const Feature1Page({Key key, this.title = "Feature1"}) : super(key: key);

  @override
  _Feature1PageState createState() => _Feature1PageState();
}

class _Feature1PageState extends State<Feature1Page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[],
      ),
    );
  }
}
