import 'package:flutter/material.dart';

class Feature2Page extends StatefulWidget {
  final String title;
  const Feature2Page({Key key, this.title = "Feature2"}) : super(key: key);

  @override
  _Feature2PageState createState() => _Feature2PageState();
}

class _Feature2PageState extends State<Feature2Page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[],
      ),
    );
  }
}
