import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:injectable/injectable.dart';
import 'package:projectbasesnow/projectbasesnow.dart';
import 'package:snowtodoapp/data/constants/constants.dart';

import 'app/app_module.dart';
import 'di/di.dart';

void main() {
  RunAppSnow(
    ModularApp(
      module: AppModule(),
    ),
    flavorValues: Constants.flavorProd,
    getItInit: () => configureInjection(prod.name),
    flavor: Flavor.production,
    errorReporter: Crashlytics.instance.recordFlutterError,
  );
}
