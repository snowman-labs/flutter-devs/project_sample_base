import 'package:flutter_modular/flutter_modular.dart';
import 'package:injectable/injectable.dart';
import 'package:projectbasesnow/projectbasesnow.dart';
import 'package:snowtodoapp/data/constants/constants.dart';

import 'app/app_module.dart';
import 'di/di.dart';

void main() {
  RunAppSnow(
    ModularApp(
      module: AppModule(),
    ),
    flavorValues: Constants.flavorDev,
    flavor: Flavor.dev,
    getItInit: () => configureInjection(dev.name),
    enableDevicePreview: false,
  );
}
