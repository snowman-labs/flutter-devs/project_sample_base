import 'package:flutter_test/flutter_test.dart';
import 'package:projectbasesnow/projectbasesnow.dart';
import 'package:snowtodoapp/data/remote/dio_client.dart';
import 'package:mobx/mobx.dart';
import 'package:snowtodoapp/flavor_values.dart';

void main() {
  DioClient dio;

  setUp(() {
    FlavorConfig(
      flavor: Flavor.production,
      values: FlavorValuesApp(
        baseUrl: "",
        features: null,
      ),
    );
    dio = DioClient();
  });

  test('Dio client test', () async {
    final resObservable = dio.get("wrongPath").asObservable();
    expect(resObservable, isNotNull);
    expect(resObservable.status, FutureStatus.pending);
    final res = await resObservable.catchError((e) {});
    expect(resObservable.status, FutureStatus.rejected);
    expect(res, isNull);
  });
}
